package com.altronis.servlet;

import javax.annotation.Resource;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


@WebServlet(urlPatterns = "/SendMessageServlet")
public class SendMessageServlet extends HttpServlet {

    @Resource(name = "topic/MyTopic")
    private Topic fooTopic;

    @Resource(name = "queue/test")
    private Queue queue;

    @Resource
    private ConnectionFactory connectionFactory;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
        Logger log = Logger.getLogger(String.valueOf(SendMessageServlet.class));

        String text = req.getParameter("text") != null ? req.getParameter("text") : "Hello World";

            //Create Connection to ActiveMQ
          try (Connection connection = connectionFactory.createConnection()) {
            connection.start();
            QueueSession session = (QueueSession) connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);

            //Send message
            try (MessageProducer producer = session.createProducer(queue)) {
              producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
              TextMessage message = session.createTextMessage(text);
              producer.send(message);
              session.close();
              res.getWriter().println("Message sent: " + text);
            } catch (JMSException e) {
              throw new JMSException(e.getMessage());
            }
          } catch (IOException | JMSException e) {
            res.getWriter().println("Error while trying to send <" + text + "> message: " + e.getMessage());
            log.log(Level.WARNING, e.getMessage());
        }
    }

}
