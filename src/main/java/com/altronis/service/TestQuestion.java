package com.altronis.service;

import com.altronis.repository.JdbcConnection;
import com.altronis.repository.MessageStoreRepository;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.lang.Thread.sleep;

@MessageDriven(name = "testMDB", activationConfig = {
  @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
  @ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/test") })
//Question4 Area Start

public class TestQuestion implements MessageListener {

//Question4 Area Stop

  final JdbcConnection jdbcConnection;
  final MessageStoreRepository messageStoreRepository;

  Logger log = Logger.getLogger(String.valueOf(TestQuestion.class));

  public TestQuestion() throws SQLException {
    jdbcConnection = new JdbcConnection();
    messageStoreRepository = new MessageStoreRepository();
  }

  @Override
  public void onMessage(Message message) {

    //I didnt understand how i could fulfill question4 where it was designated but i believe this logic block should
    //serve teh same purpose.
    if(message instanceof TextMessage) {
      log.log(Level.INFO,"Message Received is of type TextMessage...");
      try {
        sleep(10000);
      } catch (InterruptedException e) {
        log.log(Level.WARNING, e.getLocalizedMessage());
        Thread.currentThread().interrupt();
      }
    }

    for (int i = 0; i < 1000; i++) {
      try {
        doReallyComplexProcess(i);
      } catch (Exception e) {
        log.log(Level.WARNING, e.getLocalizedMessage());
      }
    }
  }

  private void doReallyComplexProcess(int i) throws SQLException {
    //JDBC answer
    jdbcConnection.insertCount(i);
    //JPA answer
    messageStoreRepository.saveMessageStore(i);
//    Thread.sleep(5000); //This sleep represents really complex code that takes 5 seconds to run and cannot be further optimised
  }
}
