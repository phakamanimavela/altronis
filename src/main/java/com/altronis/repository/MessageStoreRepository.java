package com.altronis.repository;

import com.altronis.model.MessageStoreEntity;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MessageStoreRepository {

  private EntityManager em;

  Logger log = Logger.getLogger(String.valueOf(JdbcConnection.class));

  public MessageStoreRepository() {
    em = Persistence.createEntityManagerFactory("altronis").createEntityManager();
  }

  public MessageStoreEntity findById(Long id) {
    return em.find(MessageStoreEntity.class, id);
  }

  public Optional<MessageStoreEntity> saveMessageStore(int i) {
    MessageStoreEntity newMessageStore = new MessageStoreEntity();
    newMessageStore.setMessageNumber((long) i);

    try {
      log.log(Level.INFO, "Inserting record into the table...");
      em.getTransaction().begin();
      em.persist(newMessageStore);
      log.log(Level.INFO, "Inserted records into the table...");

      if (i % 2 == 0) {
        em.getTransaction().rollback();
        log.log(Level.INFO, "Rolling back Inserted records from the table...");
      } else {
        em.getTransaction().commit();
        log.log(Level.INFO,"committing Inserted records into the table...");
      }
      return Optional.of(newMessageStore);
    } catch (Exception e) {
      log.log(Level.SEVERE, e.getLocalizedMessage());
    }
    return Optional.empty();
  }

}
