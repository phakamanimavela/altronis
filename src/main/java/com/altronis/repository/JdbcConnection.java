package com.altronis.repository;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.sql.DriverManager.getConnection;

public class JdbcConnection {
  private static final String DB_URL = "jdbc:mysql://localhost:3306/altronis";

  //  Database credentials
  private static final String USER = "altron";
  private static final String PASS = "altron1";

  private Connection conn;

  Logger log = Logger.getLogger(String.valueOf(JdbcConnection.class));

  public JdbcConnection () throws SQLException {

    try {
      Class.forName("com.mysql.cj.jdbc.Driver");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }

    log.log(Level.INFO,"Connecting to database...");
    conn = getConnection(DB_URL,USER,PASS);
    log.info("Connected database successfully...");
  }

  public int insertCount(int count) throws SQLException {
    log.log(Level.INFO,"Inserting record into the table...");
    try (Statement stmt = conn.createStatement()) {

      String sql = String.format("INSERT INTO message_store (message_number) VALUES ('%d')", count);
      int response = stmt.executeUpdate(sql);
      log.log(Level.INFO,"Inserted records into the table...");
      return response;
    } catch (SQLException e) {
      throw new SQLException(e.getMessage());
    }
  }
}
