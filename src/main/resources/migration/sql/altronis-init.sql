--liquibase formatted sql

--changeset phakamani:1

CREATE TABLE message_store  (
   id BIGINT not null auto_increment primary key,
   message_number BIGINT not null
) ENGINE=InnoDB;
