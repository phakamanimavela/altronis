package com.altronis.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class JdbcConnectionTest {

  @Before
  public void setUp() {
  }

  @Test
  public void testInsertCount_success() throws SQLException {
    JdbcConnection jdbcConnection = new JdbcConnection();
    int i = jdbcConnection.insertCount(1);
    assertEquals(1, i);
  }

}
