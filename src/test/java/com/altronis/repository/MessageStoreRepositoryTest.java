package com.altronis.repository;


import com.altronis.model.MessageStoreEntity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class MessageStoreRepositoryTest {

  private MessageStoreRepository messageStoreRepository;

  @Before
  public void setUp() {
    messageStoreRepository = new MessageStoreRepository();
  }

  @Test
  public void testSaveMessageStore_Success() {

    Optional<MessageStoreEntity> messageStoreEntity = messageStoreRepository.saveMessageStore(1);

    assertTrue(messageStoreEntity.isPresent());
    assertEquals(Optional.of(1L).get(), messageStoreEntity.get().getMessageNumber());
  }

  @Test
  public void testRollBack() {

    Optional<MessageStoreEntity> saveEntity = messageStoreRepository.saveMessageStore(2);
    assertTrue(saveEntity.isPresent());

    //the saved MessageStoreEntity should be rolled back from DB
    MessageStoreEntity messageStoreEntity = messageStoreRepository.findById(saveEntity.get().getId());
    assertNull(messageStoreEntity);
  }
}
