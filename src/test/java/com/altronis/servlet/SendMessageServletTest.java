package com.altronis.servlet;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@Ignore
@RunWith(MockitoJUnitRunner.class)
public class SendMessageServletTest {

  @Mock
  HttpServletRequest httpServletRequest;

  @Mock
  HttpServletResponse httpServletResponse;

  @Before
  public void setUp(){
  }

  @Test
  public void doGet() throws IOException, JMSException {
    Connection connection = mock(Connection.class);
    ConnectionFactory mockConnectionFactory = mock(ConnectionFactory.class);

    when(httpServletRequest.getParameter("text")).thenReturn("testText");
    StringWriter stringWriter = new StringWriter();
    PrintWriter writer = new PrintWriter(stringWriter);
    when(httpServletResponse.getWriter()).thenReturn(writer);

    when(mockConnectionFactory.createConnection()).thenReturn(connection);

    new SendMessageServlet().doGet(httpServletRequest, httpServletResponse);

    verify(httpServletRequest, atLeast(1)).getParameter("text");
    assertTrue(stringWriter.toString().contains("Message sent: testText"));

  }
}
