# ALTRON | BYTES SYSTEMS INTEGRATION
## coding challenge

### Database
there is a `docker-compose.yml` file which is located in the root of the project, 
to spin up a MYSQL db for the purposes of evaluating the solution.

``docker-compose up``

### DB migration 

this project uses liquibase to create the `message_store` table which hold
maven will create a war file on a successful build

### Deployment

I used Tomcat as a application server and deployed a war file to the application
server to functionally test the code.

### Unit test

there are unit tests for the db access sections for both JDBC and JPA.

#### disclaimer

this was the first time I used JMS and figuring out how to test is it proved more
difficult than I had thought.

### Servlet

part of the requirement was to send a message via web service: 

``http://localhost:8080/altronis_war/SendMessageServlet?text=Text``

